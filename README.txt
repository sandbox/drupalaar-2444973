REQUIREMENTS
------------

This module modifies a form of the Drupal Commerce module, which needs to be installed.


INSTRUCTION
-----------

When you enable this module, you get yet another Commerce configuration form,
where you can change the label of the Continue button for all available Checkout wizard panes.


MAINTAINERS
-----------

Current maintainer:
* Erwin Heeren (drupalaar) - https://www.drupal.org/u/drupalaar

This project has been sponsored by:
* DigiLinks
   Drupal experts with focus on content marketing, knowledge management and content commerce (publishing behind a
   paywall)